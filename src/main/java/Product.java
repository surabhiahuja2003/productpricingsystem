public class Product {

    protected final boolean imported;
    protected final String category;
    protected final String name;
    protected final Double inputPrice;

    public Product(boolean imported, String category, String name, Double inputPrice) {
        this.imported = imported;
        this.category = category;
        this.name = name;
        this.inputPrice = inputPrice;
    }
}
