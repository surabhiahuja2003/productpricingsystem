import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ProductPricing {

    public static final Double SALES_TAX = 0.1;
    public static final Double IMPORT_DUTY = 0.05;
    HashSet exemptedProductCategorySet = new HashSet(Arrays.asList("book", "medicine", "food"));
    
    public ProductPricing() {

    }

    private boolean _exemptedFromTaxes(String inputProductCategory) {
        return exemptedProductCategorySet.contains(inputProductCategory);
    }
    

    public Double computeFinalPrice(Product p) {
        Double finalPrice=p.inputPrice;

        if (!_exemptedFromTaxes(p.category)) {
            finalPrice += p.inputPrice*SALES_TAX;
        }
        if (p.imported) {
            finalPrice += p.inputPrice*IMPORT_DUTY;
        }
        return finalPrice;
    }
}
