public class Utils {
    public static String roundOff(Double val)
    {
        return String.format("%.2f", val);
    }
}
