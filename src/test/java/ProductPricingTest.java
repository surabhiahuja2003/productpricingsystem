import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class ProductPricingTest {

    //Test cases for Products which are exempted
    //1. A Book - Not imported
    @Test
    void shouldReturnFinalPriceForExemptedNotImportedProduct() {
        ProductPricing pps = new ProductPricing();
        Product p = new Product(false, "book", "Harry Potter", 12.49);
        Double actualFinalPrice = pps.computeFinalPrice(p);
        assertEquals(12.49, actualFinalPrice, 0.01);
    }
    //2. Chocolate - Imported
    @Test
    void shouldReturnFinalPriceForExemptedImportedProduct() {
        ProductPricing pps = new ProductPricing();
        Product p = new Product(true,"food", "Ferrero Rocher", 10.00);
        Double actualFinalPrice = pps.computeFinalPrice(p);
        assertEquals(10.50, actualFinalPrice,0.01);
    }

    //Test cases for products which are not exmpted
    //3. Music CDs - Not imported
    @Test
    void shouldReturnFinalPriceForNotExemptedNotImportedProduct() {
        ProductPricing pps = new ProductPricing();
        Product p = new Product(false,"Music CD", "Latest US TOP 100", 14.99);
        Double actualFinalPrice = pps.computeFinalPrice(p);
        assertEquals(16.49,actualFinalPrice,0.01);
    }

    //4. Perfume Bottle - Imported
    @Test
    void shouldReturnFinalPriceForNotExemptedImportedProduct() {
        ProductPricing pps = new ProductPricing();
        Product p = new Product(true,"Perfume", "Chanel",27.99);
        Double actualFinalPrice = pps.computeFinalPrice(p);
        assertEquals(32.19,actualFinalPrice,0.01);
    }
}